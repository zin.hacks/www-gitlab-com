---
layout: handbook-page-toc
title: "GitLab Contribute Frequently Asked Questions"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Registration

#### I'm new/I'm a manager and have a new hire. When will invitations been sent? 
New team members are sent a Contribute registration email and TripActions invite on a weekly basis. New team members cannot register or book travel before their first day, which is when they get access to their GitLab email. The registration email and TripActions may already be in their inbox, or they will receive them within a few days of their first day. New team members will have two weeks to register and book their flights.   

##### Team members who have a signing date on or before Mar. 6, 2020 will be eligible to attend Contribute 2020 so that travel and accommodation can be arranged no less than two weeks prior to the event.
{:.no_toc}  

#### I am not sure I can attend just yet, what should I do? 
You may have 30 days or less from the time you receive your registration email to register for Contribute. Your exact deadline time frame is listed in your registration invitation email. If you'd like to request an extention, please email `contribute@gitlab.com` with information on why you'd like more time to decide.

#### Do I need to book a ticket for the event? 
Yes, we need to know you will be coming and when. Please do so sooner than later even if you think some details might change. It helps us plan for headcount and room allocations. 

#### How do I modify my registration? 
There is a link to `Manage Registration Details` in the confirmation email sent after you register. 

#### How do I deregister from Contribute?
We're sorry to hear you won't be able to join us. To deregister find the confirmation email sent after you registered, and click on `Manage Registration Details`, that will allow you to cancel your registration.

If you don't have access to that email, or if you need to deregister your SO as well, please email `contribute@gitlab.com` and the team will take care of it.

## Booking travel

We’ve signed on with TripActions for all team travel. For Contribute you will receive a separate invitation email with the specific budget and travel dates for Prague directly from Trip Actions. However, since there may be cheaper flights outside of TripActions, please do a search on other websites, such as Google Flights, airline websites, and Skyscanner, before booking. If you find a cheaper flight elsewhere, please see [Booking travel outside TripActions](#booking-travel-outside-tripactions) below for more information.

Please use the invitation email link to book your flight in TripActions for correct finance tagging.

### Deadline to book travel 

Please see your registration email to determine your booking deadline.

### Budget

All amounts are listed in United States Dollars (USD). Convert the amount to your local currency if you don't use USD.
- $1200 for team members in the US/Canada.
- $200 for team members in EMEA
- $1300 for the team in APAC and other regions such as Central and South America or Africa.

Your email invite is linked to the correct budget for your location.

`**All travel budget allocated is not applicable for guest travel expenses.**`

The [handbook policy of $300 towards an upgrade on flights](/handbook/spending-company-money) of 9+ hours does not apply to Contribute*.

_*However, if your travel time is 24+ hours or includes 3+ connections please feel free to expense up to $300 towards an upgrade (the $300 is for the round trip, not each way), if you are traveling over 30 hours please let us know asap and we can arrange for you to arrive a day earlier.
If your height is greater than 6'5", please seek approval from your manager first if you would like to upgrade your flights, as the additional cost will come out of your team's budget._

Please see the [Expenses](#what-expenses-are-covered-for-team-members) section below for more on what expenses are covered for team members.

### Using TripActions

If you’re logged in, you can find the tab “Invitations” in the top menu where you can again find the Prague link to book. Please make sure to use this for correct finance tagging.

### Booking travel outside TripActions

If you've found a cheaper or more direct flight outside of TripActions, you can book in two ways:

Book on your personal credit card and submit for reimbursement. When self booking you can only expense up to the max in your region. Make sure to use the tag "Prague Contribute 2020" in the "classifications" field in Expensify.
You can contact TripActions directly, provide them with the flight information, and ask that they book the flight on the company card.

If you'd like to travel by train, you can use your local railway website to book your travel. Please feel free to reach out with any questions and note that the budget will be the same as stated above for EMEA.

For anyone making any sort of booking outside of Trip Actions, please make sure to email your full itinerary confirmation to contributetravel@gitlab.com as soon as you have booked it.

### Travel FAQs

#### What if I can't find a flight within the budget for my region? 
Please keep an eye on prices until you are about **2 weeks away from your booking deadline**. You can set up an alert to be notified in price changes on Google Flights. Please do searches outside of TripActions on Google Flights, airline websites, and other travel sites to see whether there's a price difference.

If you are still unable to find something that works, please reach out to `contributetravel@gitlab.com`, and we will do our best to accommodate you.

When you email the Contribute team, please attach screenshots of your research on both TripActions and external sites. **Please note that you must reach out to the Contribute team before booking your flight and gain approval for booking outside of your budget. Failure to do so may result in covering the overage out of pocket. Your manager will also be contacted in the event you book outside policy.**

#### What about travel to and from the airport?

Please see the [expenses section](#what-expenses-are-covered-for-team-members) below.

#### What if I don't have a passport/need to renew my passport? 
If you don't have a passport yet, please enter `000000000000` for now. Apply for a passport **ASAP**. When you have it come back to modify your registration and enter the number.

If you are renewing your passport, enter your current number and come back to modify it when you have the new passport.

Please see [Passport costs](#passport-costs) below for guidance on expensing your passport application fee.

#### I need an invitation letter from GitLab to apply for a visa. Where can I get one? 

Also, can I get one for my SO and/or children? 

Please check the [handbook page on visas](/handbook/people-group/visas/#prague-contribute-2020-visa-invitation-letter) for details about getting an invitation letter.

#### What if I want to book my travel to/from a city other than Prague? 
If you want to extend your trip to visit another city in the area, it's fine to book and expense your travel to/from another destination, provided it costs no more than traveling directly to/from Prague and falls within the budget for your region.

#### What if I want to extend my trip and travel on different days? 
If you want to extend your trip, it's fine to book and expense your travel on different days, provided it costs no more than traveling directly to/from Prague and falls within the budget for your region.

#### When should I arrive/depart? 
Plan to **arrive on March 22nd** (this means you may need to leave your home on the 21st). This is arrivals day and there are no planned activities for this day. Even if you arrive at 11:45pm you will not miss out on anything and the team will be there to check you in.

If you arrive early on the 22nd, you may not be able to check into your hotel room right away, but the team will be there to greet you, and the hotel can hold your luggage so you're free to explore the city.

**March 27th** is the departures day; you can leave at any time on that day as the event concludes the night before and no activities are planned for the 27th.

#### I found cheaper flights. Can I expense, or do I have to use TripActions? 
If you find flights outside of TripActions that are cheaper, you can purchase and expense the flight, see [Booking travel outside TripActions](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/blob/master/booking-travel.md#booking-travel-outside-tripactions).

#### Can I use the $300 upgrade for my flight? 
The [handbook policy of $300 towards an upgrade on flights of 9+ hours](/handbook/spending-company-money) **does not apply to Contribute**.

However, if your travel time is 24+ hours or includes 3+ connections, please feel free to expense up to $300 towards an upgrade (i.e. $300 total, not per flight).

If you’re traveling 30+ hours, please let us know asap, and we can arrange for you to arrive a day earlier.

If you are taller than 1.95 m or 6'5", you can upgrade to Economy Plus. Please email `contributetravel@` before booking.

#### I need a Schengen visa for Contribute. What happens if my visa application is declined and I need to cancel my travel plans? 
First of all, we hope this doesn't happen to you! But if it did, any cancellation fees would be taken care of by GitLab.

This is also applicable if you are unable to apply for visa before the deadline for booking flights. You can book your tickets before the deadline if you are unable to apply for a visa at that time. That way, you get better rates on your tickets. We can cancel the tickets based on your status of Visa later, and GitLab will take care of any cancellation fees.

Please see [Visa application costs](#visa-application-costs) below for guidance on expensing.

#### I'd like to travel by train to Contribute but this won't be within budget for my region. What can I do? 
Thanks for being eco-conscious! Please email `contributetravel@gitlab.com` with the price difference and we will do our best to accomodate you. 

#### What if I need to come early for a team meeting?
Once your manager notifies the planning team at `contribute@gitlab.com` we'll make arrangements for lodging. 

## Guests

#### Can I bring a guest? 
Yes, GitLabbers may bring one (1) guest, and they're encouraged to buy a ticket to attend. This way they will be able to join in on all activities, meals, excursions, workshops, and more. Any guests that do not have a ticket will not be able to participate in any Contribute activity.

#### Can my guest/child attend without a ticket to Contribute? 
This is discouraged for a few reasons. The purpose of Contribute is to connect with our team face to face, as we only gather in person once every nine months. If your guest is unable to join in on any activities, you may find yourself torn between spending time with other GitLabbers and hanging out with your guest, which could mean that you miss out on bonding with teammates.

It is also not guaranteed that you will get a private room if you request one (see the [room info page](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/blob/master/room-info.md)). The safest way to being your SO to Contribute is to buy them a ticket. The cost of a private room to share with your guest is included in their ticket.

That said, we understand that Contribute is a long time away from your family, and if the only way to attend is to bring our child/guest/both, then we won't charge a guest ticket.

**Please note we cannot accommodate anyone under 18 at any Contribute activity, such as meals, excursions or workshops.**

#### How do I register my guest? 
If you are buying a ticket for your guest for Contribute, you will need to register them separately. 

#### When is the latest I can register my guest to attend? 
Your guest has the same deadline as you do. 

#### Is my guest's ticket refundable if they have do to cancel for some reason? 
It is refundable up to **45 days before** Contribute. 

## Accommodation

During registration, you can express your rooming preferences. Final room assignments will be shared after registration closes, around mid-February.

### Hotels

We will be staying at the Hilton Prague (Pobřežní 1, Prague, Czech Republic, 186 00) and Hilton Prague Old Town (V Celnici 7, Prague, Czech Republic, 110 00).
You will not necessarily be in the same hotel as the rest of your team. 

### Sharing a room

There is no additional cost if you opt to share a room, and you can either choose a roommate 
(this can be any other team member, but you should ask them if they are happy to share with you before putting their name down!)
or a roommate will be allocated to you based on the preferences you note during registration. We encourage you to schedule coffee chats with your roommate before Contribute.

### Private rooms

#### With your guest

If you buy a Contribute ticket for your guest, you will automatically get a private room to share with them. The cost of this is included in your guest's ticket.

If your guest is joining you, **it is strongly recommended that they have a ticket for Contribute**, rather than joining independently and sharing a single private
room with you. The intention of Contribute is to be together with our team for IRL networking, relationship building, and experiences both with team members as a whole, 
and specialized department activities throughout the day and some evenings. If a guest ticket is purchased, they are welcome to attend any and all elements 
of Contribute (workshops, food and beverage, events, excursions, lodging, etc.). If your guest doesn't have a ticket, they will not be able to join any
element of Contribute, which might be isolating for them and could lead to you feeling torn between participating with the team and spending time with your guest. 
**Please note that only registered guests can be accommodated on shuttles to/from the Prague airport. If you are bringing unregistered guests, you must arrange for your own transportation
at your own expense.**

#### On your own

If you want to have a private room to yourself or to share with your guest who doesn't have a Contribute ticket, there is an additional cost of US$500. 
This is a fixed rate for GitLab and will not change, regardless of fluctuations in the listed price on the hotel's own website.

There may not be sufficient private rooms to accommodate everyone
who wants one, so please note this is not guaranteed. It is best to register early as it is first come, first served. We will also take personal/medical 
needs into account if the demand for private rooms outstrips supply.

We will notify team members who have requested a private room closer to the event. (We expect this to be [around mid-February](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/issues/17#note_261359546).) We will also let you know when and how to pay the fee closer to Contribute.

#### When do I pay for a single room? 
After the first wave of registration closes on December 1, 2019, we'll send invoices to those who have selected a single room. 

### Other lodging options

Team members are not permitted to stay in other accommodations during Contribute, such as Airbnbs or other hotels due to security reasons. Furthermore, the goal of Contribute is to build bonds with the GitLab community, which is easier to accomplish when we're staying at the same locations.

### Looking to stay additional days at the Contribute hotel?

If you want some extra time to explore Prague, this year we have added an option to select additional night stays before or after Contribute 
in the registration process. This will be an out-of-pocket cost of 4400 CZK + VAT (approximately $241 USD; this includes breakfast but not taxes) per night, 
but we will confirm the registration on your behalf. You will not need to check back in or out, and payment will be due upon checkin.

## Attending Contribute

#### Is it possible to attend Contribute for a shorter amount of time? 
While we encourage everyone to attend the entire event because the program is crafted to ebb and flow with various learnings and differentiated interactions - both departmental and cross functional - we understand that this is a long time away from family and other priorities, so we're able to accept partial bookings as long as team members can **attend for at least 2 full days excluding Arrivals Day**.

#### What if I have to leave early?

In the case of an emergency that requires you to leave Contribute, please reach out to an Ambassador. We'll help find transportation and provide any other support you need. You won't be expected to reimburse GitLab for anything.

## What to expect at Contribute

#### How much free time will there be during Contribute? 
You can count on having free time during Arrivals Day and in the evenings. You’ll have a packed schedule the rest of the time, including excursions, workshops, and keynotes. Please note that events are not mandatory, so if you’re not up for an excursion or a workshop, you’re not obligated to attend. However, the goal of Contribute is to build bonds with each other and cultivate a strong community, so if you decide not to attend an event, we hope that you spend that time recharging or getting to know others.

#### What is Team Day? 
Team day is designated time where functional groups and teams can organize an activity together. It is best to organize with the group you work with day to day. This can be a meeting during the day with a stage (multiple teams) and then a dinner with a team (smaller group). Or it could be a meeting with your team (like Marketing Operations or Field Marketing) and a dinner with a larger group (all of Marketing).

If you’re leading a team gathering, please see the [Team time faq](https://gitlab.com/gitlab-com/marketing/contribute/contribute-2020/-/blob/master/team_time.md#faq-team-time) for more details.

## What expenses are covered for team members?

#### Airfare or train tickets
Please see [Booking travel](#booking-travel) above.

#### Is checked luggage reimbursable? 
On TripActions, most airlines provide an option to pay extra for checked luggage. GitLab will reimburse the cost for adding one checked bag at a standard weight (only if the existing booking doesn't permit checked luggage), up to a maximum $100 USD total.   

If you booked outside of TripActions and the booking does not include any checked luggage, please feel free to expense the cost of adding one checked bag at a standard weight. If possible, please try to do so ahead of time, as it is often far more costly to do this when you reach the airport. 

### Getting to and from your home airport
Parking and mileage*/taxi/public transport to and from your home airport/train station can be expensed up to $80 USD total (this is on top of the limit set for flight costs in your region). 

*Please see [Spending company money](/handbook/spending-company-money/#expenses) for guidance regarding expensing mileage.

If you are extending your trip and traveling earlier or later than the official Contribute dates, you may still expense these travel costs associated with getting to/from your home airport on your traveling days.

Please use the `Prague Contribute 2020` classification in Expensify.

### Getting to and from the Contribute hotel

#### Arriving by air
GitLab will arrange transportation to and from Prague airport with scheduled group transfers. If you are arriving before March 22nd or leaving after March 27th we **won't reimburse you** for your transport to or from the airport in Prague, unless you have an exception explicitly agreed upon with the Contribute team. 

#### Arriving by train
Watch this space – instructions coming soon!

#### Exceptions
- If you are coming in early as part of Support, the Contribute event team, or as a Contribute Ambassador, you may expense your transport to the Contribute hotel with the `Prague Contribute 2020` classification in Expensify.
- Those coming in for extended team meetings should mark those transportation expenses towards their team when expensing, rather than using the Contribute classification. 
- If you have to leave before March 27th, you may expense up to $25 to get to Prague airport as long as there are no transfers booked for you. 
- If you choose not to make use of GitLab-arranged transfers on your arrival/ departure date you **cannot expense** your transport. 

**Example 1**: You are flying in early to Prague to spend a few days sightseeing before arrivals day. You may expense your transport to your home airport, but not transport on arrival in Prague. You will need to make your own way to the Contribute hotel at your own expense. On departures day, GitLab will arrange and pay for a group transfer from the hotel to the airport. When you arrive back in your home country, you may expense your transport home from the airport.

**Example 2**: You arrive on arrivals day in Prague, but are staying on afterwards to travel elsewhere in Europe. You may expense your transport to your home airport. GitLab will arrange and pay for a group transfer to the hotel on arrival in Prague. You will then need to make your own way to the airport (wherever you are flying home from) at your own expense. When you arrive back in your home country, you may expense your transport home from the airport.

### Passport costs
If you need to get a passport or renew it *specifically* for traveling to Contribute, you can expense up to $75 USD to cover the cost of this process. If the costs are higher, GitLab will only reimburse up to $75 USD.

To take advantage of this perk, email `contribute@gitlab.com` with details for approval and include your manager in cc. Once approved, submit for expense reimbursement and mention that the expense has been approved.

### Visa application costs
Please [check first if you need a visa to visit Prague](/handbook/people-group/visas/#arranging-a-visa-for-travel-)! 

If you do need one, you may expense the visa application fee, transport to the embassy or visa application center, courier fees for return of your passport, and accommodation if you need to travel to another city to make your application. Please use the `Prague Contribute 2020` classification in Expensify.

Please take care to note what documents are needed for your visa application and always follow official guidance from the embassy or visa application center regarding timelines. **GitLab will not reimburse you for additional trips to apply if you haven't prepared adequately.**

### Accommodation
Hotel nights/tax have already been paid for by GitLab during Contribute. Each person will need to check in and provide a credit card for incidentals, but won't be charged unless there are any (e.g. room service). Basic internet is included in the room. When sharing a room, usually the first team member to arrive will be prompted to put down their card. Communicate with your roommate and discuss how to split any bill that may arise from room incidentals.

### Food and drink 

#### Meals
GitLab has arranged meals for everyone attending on the following dates: Monday, March 23rd - Thursday, March 26th. If you choose to not consume what we have prearranged or go out on your own, you cannot expense separate items/ meals. 

Example: We will have coffee at the hotel every day; if you decide to buy a coffee offsite that will not be expensable. 

On arrival and departure days (March 22 and 27) you may expense up to $65 USD towards meals. This $65 USD is for each day and applies to GitLab team members only. If you are traveling to Contribute on different days, you may expense this amount on your travel days only.

Example: As your Department is meeting early on March 22, you are traveling to Prague on March 20 to get acclimated with the new time zone. Then, after Contribute, you are leaving to spend one week in Greece until April 14th. You would still be able to expense $ 65 USD in meals on March 20 and April 14.

#### Drinks
GitLab will have a few hosted bars. If you order something outside of these hours or off menu these drink cannot be expensed or put on the GitLab tab. 

If your team has a happy hour or drinks with **your team dinner that will be handled by your team lead** – please ask them for their parameters on ordering. 

### What's **NOT** Covered
* Accommodation on any additional days outside the program (unless you are a Contribute Ambassador or Support team member joining early or staying on to provide coverage, or otherwise have prior permission from the Contribute team)
* Food on any additional days you stay outside the program or offsite (apart from the team dinners)
* Room service, mini/bar, late night eats/drinks, etc. (i.e. food and beverage outside of what is officially being provided during Contribute)
* Meals and beverages, activities, or excursions for SOs or any guest joining you without a Contribute ticket
* Transportation costs that aren't to/from the airport and hotel (e.g. do not expense cab rides from after-hours activities back to the hotel)
* Laundry
* Incidentals at the hotel (each person needs to put down their own card)
* Any snacks or souvenirs you pick up during excursions or offsite events unless provided by our team 
* Your costume for the final party
* Additional visits to the embassy or visa application center for your country as a result of not being prepared with the correct documention or attempting to apply too soon
* Global Entry or equivalent
* Anything not listed in the [What expenses are covered](#what-expenses-are-covered-for-team-members) section. When in doubt, go with the assumption that it can't be expensed, and use your best judgment.
* **Please note if you get anything delivered to the hotel, they charge a handling fee for all packages received.** This expense will be charged to your room.

___
We want to ensure everyone has a safe and successful week and that everyone feels like they are getting their needs met, so if you have any questions about the policies above please reach out to contribute@gitlab.com.

