---
layout: job_family_page
title: "Channel Programs & Enablement"
---
## Director of Partner Programs & Enablement
The Director of Partner Programs & Enablement helps manage and support the growth of the channel business. They are responsible for the day to day channel program development/operation as well as the execution of our partner program. The Director, Channel Programs and Enablement reports into the VP, Channel Sales and works cross-functionally with marketing, sales, product, channel ops, and legal to integrate channel programs throughout the business.

### Responsibilities

* Responsible for the design, build and management of partner programs to support Gitlab Channel working in collaboration with the Channel Sales team
* Develop Partner Program collateral/documents and onboarding tools to enhance overall partner experience and communicate to partners
* Develop Partner Portal; leveraging SFDC & PRM - working with marketing & Channel Ops 
* Provide management of Partner Portal to create an engaging platform for partners to interact with Gitlab (lead registration, opportunity tracking, commissions, etc)
* Work with channel operations/IT to implement channel strategy, data/reporting, system workflows, and overall operational governance for the program
* Design Channel Enablement Certification & Badge Model.  Work closely with Channel Marketing, Channel Services, Customer Success & Sales enablement on partner enablement tools/training and implement as part of overall program
* Manage and provide administration of partner agreements/partner contracting working in coordination with legal
* Communicate channel programs, strategies & events and updates internally to drive alignment throughout the business
* Work cross-functionally with marketing, product, legal and finance to continue to implement and grow partner programs and ensure alignment across the business
* Work with partners to solicit feedback on existing programs/processes and implement changes (Partner Advisory Councils, via channel teams, etc) 
* Build a team to effectively manage channel programs, enablement, portal, comms & MDF/incentive management.  

### Requirements

* Prior experience in running Partner Programs, Channel Enablement, Channel Marketing and/or Channel Development in the services/IT space
* Thrive in a fast-paced, ever-changing environment
* Problem solver and self-motivated
* Exceptional oral and written communication and presentation skills required
* Strong interpersonal skills
* Ability to conceptualize and sell ideas internally
* Excellent time-management and multi-tasking abilities
* Innovation and the ability to gain consensus is necessary
* Works extremely well as a member of a team, but also excels as an individual contributor
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)
* Ability to use GitLab

# Specialities

## Channel Services Manager

The Channel Services Manager will serve as a primary point of contact for Gitlab’s top services partners and will ensure that services partners are trained and certified to provide professional, consulting, managed  & advisory services for Gitlab customers. This individual will help Gitlab manage the evolution of its professional services offerings via partners to drive usage, adoption of the complete GItlab platform and expansion into the customer. 


### Responsibilities 

* Provide a channel of communication between the partner and Gitlab to ensure smooth and effective customer implementation processes, seamless Managed Services and value-driven strategic advisory services.  
* Identify, define and assist in building services offerings and/or practices at the partner that support all levels of the Gitlab Customer Service cycle. 
* Content and Tools Creation - Support the Gitlab Enablement, Professional Services & Customer Success teams in building and improving tools and processes that promote mutual success.
* Working with the Channel Sales Managers, drive and facilitate partner certifications through eLearning, instructor-led training, boot camps and ongoing certification maintenance.
* Find and recommend new Services Partners in collaboration with corporate and regional Channel Sales Managers 
Ensure our partners have the skills, training, and expertise to implement, operate  and support Gitlab solutions.
 
### Requirements

* Must demonstrate the capability to excel within a cross-functional team environment.
* Experience building channel services & practices in SaaS/subscription models.
* Strong presentation and written communications skills. Previous experience enabling partners to deliver services that grow revenue, expand customer footprint and drive renewals.
* Excellent strategic planning, project management, communication and presentation skills.
* 7-10 years of working experience in partner management, training, product marketing, professional services, or product management in the high-tech industry
* BS/BA required or equivalent experience; MBA a plus
* Experience in the DevOps space a plus
 
## Senior Channel Program Manager

The Channel Program Manager will be responsible for the design, management & execution of Gitlab’s Channels & Alliances Partner Programs.  In addition to the partner programs, the Channel Programs Managers will manage deal registration, recruitment and partner success programs and manage the partner advisory council.  
 
### Responsibilities
* Collaborate across Channels & Alliances teams to gather requirements for Programs
* Design, build and manage Partner Programs including the Program Guide, MDF Program, Reseller, Services, Alliances, Deal Registration Program, etc. 
* Design program guides that details requirements and benefits, terms and conditions, etc. 
* Design, build & launch partner portal.
* Manage all communications to the channel via portal, newsletters, social channels.  
* Design and launch field / channel rules of engagement.
* Design & launch field enablement on Partner Program, Rules of Engagement, Deal Reg, How to Partner, etc.
* Collect and manage feedback from channel partners into the ongoing roadmap for the Channel Program.
* Work cross functionally with sales operations, marketing, finance, enablement and technical teams to secure, operationalize and manage all the committed benefits to the partners.  
* Document policies and processes to support all aspects of the program and enables global execution.  
* Establish and track metrics of the program, run a quarterly compliance process with the Channel & Alliances field.  
* Working with your peers, marketing & Sales operations

### Requirements
* Bachelor’s degree with 5+ years experience in channel programs.  
* Experience building and managing a partner program in DevOps space 
* Excellent communication, analytical and organizational skills.
* Execution & revenue focused
* Great project & program management skills, ability to leverage, manage & lead virtual teams
* Successfully designed, built and managed partner portals.


### Performance Indicators
[Sales KPI's](https://about.gitlab.com/handbook/business-ops/data-team/kpi-index/#sales-kpis)

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
1. Phone screen with a GitLab Recruiting team memeber 
2. Video Interview with the Hiring Manager
3. Team Interviews with 1-4 teammates 
Additional details about our process can be found on our [hiring page](/handbook/hiring).
