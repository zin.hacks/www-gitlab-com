---
layout: handbook-page-toc
title: "Acquisition Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This is a detailed view of our acquisition process. For more information about
our acquisitions approach visit our [acquisitions handbook](/handbook/acquisitions).

### Acquisitions Deal Funnel
We conduct a comprehensive screening of our industry ecosystem in order to identify potential acquisition opportunities. Below is an overview of our target funnel.

| Step | Success rate/volume |
| --- | ---|
| Potential target companies | 1000/year |
| Strategy fit | 30% |
| Deal term fit | 15% |
| NDA | 100/year |
| Code screen share | 50% |
| Founders technical interview | 70% |
| Resume review of all people | 70% |
| *Optional step*: sample interviews with non-founders (will increase LOI success rate) | |
| Deal terms discussed and socialized |
| LOI | 15/year |
| Review all code | 90% |
| Interview all people | 60-90% |
| Acquisition offer | 12/year |
| Acquisition closed | 10/year |

## Acquisition process
The process is comprised of five key stages:
1. Exploratory
1. Early diligence
1. Business case
1. Confirmatory due diligence
1. Integration

### Exploratory stage
1. Intro call: The purpose of this 30-min call is to:
    1. Learn about your company including team, products, financials, and funding
    1. Review the expectations and process noted on this page
    1. Start discussing which features could be built into GitLab
    1. Discuss which GitLab product category the team could join as a whole    
    1. Discuss deal terms as noted on the [acquisitions handbook](/handbook/acquisitions)
    1. Answer questions your team may have
Details from this call should be collected following the [Initial Acquisition Review Template](https://docs.google.com/document/d/1RekjfQj89pIV7DZzeNZ00HYOwqlqlDFm7Gy4yZET9rc/edit?usp=sharing)(a GitLab internal document).
TARGET TEAM: Ahead of the product call please review our [roadmap](/direction/) and outline which of your current and future product features can be implemented into GitLab's product categories. Outline a simple integration timeline for those features, considering an MVC release on the first month after joining GitLab and monthly releases following with quick iterations.
1. Create a new, private Slack channel. Format: `#acq-company_name`. Add VP Product Strategy and the relevant product category director(s).
1. Add template WIP Business Case to the top of the acquisition gdoc and start filling out the details
1. Set up initial product call: start product diligence and discussion of which features could be built into GitLab and into which GitLab product stage. Discuss strategy fit to GitLab's [product roadmap](/direction/).
1. Internal review: preliminary assessment of product and technology fit of the potential opportunity as well as integration options into GitLab. If the product lead is supportive of moving forward towards developing a more in depth business case, determine if the proposed acquisition is aligned with current product priorities and planned headcount allocations. Otherwise, set up a discussion with VP of Product before proceeding with further diligence.

### Early Diligence
1. Select [code name](/handbook/acquisition-process/#acquisitions_are_confidential) to use instead of target company name. Update Slack channel: `#acq-code_name`.
1. Sign a mutual NDA as linked on our [legal handbook page](/handbook/legal/).
1. [Form the acquisition team](/handbook/acquisition-process/#forming_an_acquisition_team).
1. Internal acquisition champion - every acquisition needs a champion; someone who is advocating for the acquisition, helping drive the acquisition rationale, and seeing its successful integration. For most acquisitions that fit our [approach](/handbook/acquisitions), the champion will come from GitLab's Product team, at the Director level. For other acquisitions, champions may come from engineering or possibly other functions.
1. Preliminary financial & legal diligence - list of preliminary documents to share with GitLab:
   1. Financials
   1. Tax returns
   1. Employee roster with: employee name, title, role, tenure, years of experience, location, salary, LinkedIn profile, programming languages proficiency
   1. Employee Résumés and/or LinkedIn profiles
   1. Employee agreements and PIAA
   1. Customer list with name, monthly revenue, contract termination date and any other fields if relevant.
   1. Vendor list with monthly spend
   1. Asset list
   1. Any assets that are needed for the business and will be part of the acquisition
   1. Assets excluded from the acquisition
1. Early technical diligence:
   1. In case the target company has open source components, the respective Dir. Engineering (dependent on GitLab stage) will start an early code review to determine: code quality, development practices, contributions and more. That should be turned around within 2-3 business days.
   1. Hands-on product and code screen-share session (2 hours): the technical lead, as assigned by the respective Dir. Engineering, together with the respective Dir. Product will lead a screen-share session aimed at a hands-on validation of the product functionalities and an overview of the code.
   1. Founder technical interviews - founders will go through two rounds of interviews to assess technical and cultural alignment.
1. Resume review - Review of all employee resumes
1. Compensation review to identify any gaps and possible flags led by the HR Business Partner
1. Optional interviews for the key technical employees - to increase the success rate of the deal post-LOI we recommend conducting interviews for the key technical employees identified before signing the LOI. This will greatly reduce the likelihood of personnel gaps becoming a blocker during the Due Diligence stage. The interviews will include a technical interview and a manager interview as detailed in the Due Diligence stage below.
   1. The key technical employees are those identified as critical to the success of the acquisition, the proposed integration plan and the future of the team at GitLab post integration.

### Business Case stage
1. Product integration strategy: the internal acquisition champion will formalize the integration strategy with a focus on feature sets/functionalities:
   1. What we keep as-is
   1. What we reimplement in GitLab
   1. What we discard/EOL
   1. Critical for user migration
   1. Target [product tiers](/handbook/marketing/product-marketing/tiers/) in GitLab
   1. Barriers for implementation
   1. Deal milestones:
      1. Typically we aim to set 3-4 milestones, to provide a concise set of goals which should cover the bulk of our product interest in the target company
      1. Integrated within 6 months:
         1. 6 months is an optimal timeframe which isn't too far sighted where possible changes in priorities could consequently lead to us negating our interest in said milestones. This could further lead to complexed legal scenarios where payments are subject to milestone completion but if those were changed then  circumstances are different.
         1. Will help establish focus on both acquired target and our product team
         1. Be able to complete payouts to the target's entity and shut it down sooner
      1. First milestone shipped within 30 days of joining GitLab:
         1. Critical to adopting our culture and successful future integration of the target's engineering team in GitLab.
         1. Allows us to show early fruits of the acquisitions soon, aligned with our value of iteration.
1. 1. To determine the deal ROI, the acquisition team will perform the analysis using the [cost-revenue acquisition calculator](https://docs.google.com/spreadsheets/d/1ke36-mtEi8MhfMKXpYGMRP6H3HH6MimxXt86Zv_QkzM/edit#gid=0) (_internal_ GitLab document). Make a copy of the master cost-revenue acquisition calculator file and save it in the relevant project folder in Google Drive before making changes to the file.
2. Present business case for internal review of the acquisition proposal.
1. LOI and Exclusivity Agreement: Using the [LOI template](https://docs.google.com/document/d/1bSnGDPS94BGd8C9MfUvJA34X-AcpVp7wDekcBIL_Adw/edit)(_internal_ GitLab document) and the [Exclusivity Agreement](https://docs.google.com/document/d/1QhEVaLYjytxtxCKDc6eoE4Ix9lelb3dOlKgLEK8lKOg/edit?usp=sharing), assuming all terms have been reviewed, the acquisition lead will both agreements for digital signing.

### Confirmatory Due Diligence
1. To kick-off the Confirmatory Due Diligence stage, email the target company with the following clarifications and information:
   1. All employees and their profiles will be reviewed by the GitLab team
   1. The employees who will be invited for an interview process will go through GitLab's standard interview process
   1. Key employees who were interviewed during the Early Diligence stage may go through further interview rounds as determined by the GitLab team to qualify for a role at GitLab
   1. All employees must identify an open vacancy at GitLab which they think best matches their professional profile. This will be shared in a spreadsheet gathered by the target's CEO.
1. [Technical diligence](acquisition-process-technical-diligence/)
1. Financial & legal due diligence
1. The progress of the diligence will be synced on a regular stand-up call for the acquisition team
1. Final internal review call with the acquisition team to recap the acquisition as a whole and present a final recommendation

### Integration
The Corporate Development team will be responsible to oversee and facilitate the integration of the acquisition post-closing, working closely together with the respective organizations in GitLab, namely: Legal, Product, Engineering, and Finance. The DRI for the integration stage is the Sr. Director, Corporate Development. This will consist of several aspects and [best practices](https://www.mckinsey.com/business-functions/strategy-and-corporate-finance/our-insights/how-the-best-acquirers-excel-at-integration):
1. Operational shutdown (if appropriate):
   1. Execution of customer sunset plans
   1. Shutdown of target company and its entities
1. Alignment on objectives and goals:
   1. Incorporate the integration plan into GitLab's roadmap
   1. Introduce deal milestones into respective departmental OKRs
   1. Create and update product vision in case a new product area is added to GitLab
1. People & Culture:
   1. Cultural integration plan, analyzing similarities and differences in the two companies' cultures
   1. Onboarding of acquired team members
1. Performance tracking:
   1. Identify and continuously revisit business performance metrics to track the deal's added value and ROI over time
   1. Delivery against product plans and OKRs

## Forming an acquisition team
An acquisition team will consist of the following GitLab functional team members:
1. Corporate Development acquisition lead
1. Sr. Dir. Corporate Development
1. VP Product Strategy
1. Dir. Product Management
1. Product Manager
1. Dir. Engineering
1. Engineering team member
1. Finance / accounting team member
1. Legal team member
1. HR Business Partner

To assign the product manager, after the product call or as soon as it's clear which product category the features will be implemented into, contact the category product director for the assignment.

To assign the engineering team member, contact the engineering manager of the relevant category for assignment.

### Acquisition team responsibilities

| Function | Role | Deliverables |
| -- | -- | -- |
| Corporate Development | 1. Main POC for acquired team 1. Identify potential areas for integration 1. Create case for acquisition and customer transition story 1. Integration | 1. Business case with deal structure | 
| Product  | 1. Outline current product features to be implemented into GitLab 1. Outline potential future functionalities to be built into GitLab after the integration period | 1. Integration strategy|
| Engineering | 1. Technical diligence | 1. Code quality review 1. Integration strategy validation - feasibility and timeline |
| Finance | 1. Lead financial diligence 1. Validate business case and deal structure | |
| Legal | 1. Review entity, assets and existing agreements 1. Evaluate sunset and customer transition path | 1. Term Sheet 1. Acquisition agreement|
| HR Business Partner | 1. Lead the compensation review 1. Lead the interview process during the early and due diligence stages to completion |

## Acquisitions are confidential
At GitLab, we treat all acquisition discussions as confidential and share any information internally on a need-to-know basis. If you're part of an acquisition Slack channel, Google Doc, or other internal GitLab discussion and would like to invite another GitLab team member to one of those, please confirm with the acquisition lead before doing so.

To ensure confidentiality during the acquisition process, we assign code names to each potential transaction once we enter the Early Diligence stage. Everyone involved in the project should use the code name in place of the actual company name in all communication about the deal until it is publicly announced.  
