---
layout: markdown_page
title: "Jason Yavorska's README"
---

Hi! Thanks for visiting my README, inspired by [Sid Sijbrandij](/handbook/ceo/), [Eric Johnson](/handbook/engineering/erics-readme/), and others at GitLab who have done the same. You can see the rest of the Product team ones [here](/handbook/product/readme/)

## About me

- I'm a US citizen living in the Netherlands as an immigrant. I love it here and hope to stay forever. I've even learned Dutch! My partner is Ukrainian/Russian, so I'm also learning Russian but am at the very beginning levels. 
- I have two kids, a boy and a girl. My son lives with me in the Netherlands, and my daughter lives with her mother in Portland, OR.
- Because of this, but also because I love travel, I'm very frequently (up to about half the year!) working remotely from some new place.
- I've mostly worked at mid-size companies turning on the afterburners for their next level of growth, just like GitLab. You can see my career journey on my [LinkedIn](https://www.linkedin.com/in/jlenny/)
- My entire career has been in CI/CD, from my first job as a build engineer on Windows 98, all the way to director of global release engineering & management at Nike and everywhere in between. I also spent a lot of time in the video game industry.
- My role at GitLab is [Director of Product Management, CI/CD](/job-families/product/director-of-product/), you can read about my responsibilities there, and also see the unified [CI/CD](https://about.gitlab.com/direction/cicd) direction.

## Logistics

- I try to keep to about an eight hour day because I value time with my family, and I prioritize the things that seem most important/urgent. If you think that I'm missing something important, please let me know.
- In order to work effectively with US and Europe teams, I split my day in half - typically about 4 hours in the morning, a break, and then 4 hours in my early evening. This should always be visibile in my calendar.
- I'm fine with async working and expect my requests to go onto a queue and get dealt with eventually; if something is urgent I'll call it out. If something is going to be a really long time to get to, because it isn't a priority for example, please let me know rather than just not responding and not doing it as this is harmful to trust.
- Over my time here I've been on so many different projects and roles that I was getting hundreds of passive alerts a day on various issues. Because of this I've changed my notifications to be on @ mention only - if you need me, or if you reply to me, please @ me in the message.
- You can always just be direct with me in confidence, I am very invested in doing well at work but don't take anything personally since I'm always looking to learn and grow.

## My strengths (and weaknesses)

A truism I believe is that strengths can be weaknesses when turned up to 11, so I've covered both points of view below. I try to be mindful of this and bring only the positive aspects, but I don't always succeed. Please let me know if I ever let you down so I can correct it. On the other hand, I love to help, so let me know where my strengths can help you!

- I tend to be an optimistic person and aim big then adjust for misses, rather than take a more careful and predictable approach. I try to leverage the experience of the team to make sure we don't walk into a sure failure, so if you ever think I'm being too blind to the risks please let me know - it's appreciated.
- Debating is my natural way to explore a problem. Going back and forth, looking at things from different angles, and coming up with a pragmatic solution. For people who don't work this way, it can feel like you are not being heard. If you ever feel this way know it's not my intent. I do try to be sensitive for this, but you can always tell me to slow down. Also, if you just need me to immediately make a decision and stop exploring options, just let me know.
- I have a "superpower" for organizing and finishing tasks. If you ever have a repetitive bit of analysis to do, or "human bulk update" that you need to work through, please call on me to help you. If you find yourself on the receiving end of one of my bulk updates, I apologize. I do try to give a heads up on these, but if you ever notice me blowing up your inbox it's assuredly unintentional. Ping me and I can explain what I'm working on.
- GitLab is my first remote company. I truly believe remote work changes the world for the better. That said, I'm still learning the subtleties: async communication, balancing pings vs. personal follow ups, making decisions with urgency without leaving people out, etc. If I ever strike the balance wrong, please help me by letting me know how I can do better.
