---
layout: handbook-page-toc
title: Talent brand
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

As a global, all-remote company, GitLab has a unique story to tell. 
A key part of telling that story is sharing what it's like to work here, because we wouldn't be successful without our people and our culture. 
This page outlines our approach to talent branding at GitLab.

### Roadmap

Here's an [overview](https://docs.google.com/spreadsheets/d/1FdAa90Q8L5Y0bd4jgA3MkPzjvBjYXJ7OvMj51YLPwp0/edit?usp=sharing) of the current and future projects and workstreams that are part of talent branding at GitLab. 

### Our audience

The audience we hope to reach with our talent brand initiatives is both internal and external to GitLab:

- GitLab team members
- Job candidates and future team members
- The broader GitLab community 
- People interested in remote work 

## GitLab team member value proposition

We've recently defined our team member value proposition, and you can find it on the [GitLab Culture page](/company/culture/#life-at-gitlab). We're working on bringing it to life across our digital channels in Q3 to better tell the story of what it's like to be part of our global, all-remote team. 


## Digital channels for talent branding

### Jobs site

Our [GitLab jobs site](/jobs/) is where candidates can find information about working at GitLab, along with links to browse and apply for our vacancies. 

### Social media

We incorporate content about hiring and our culture on GitLab's [social media](/handbook/marketing/corporate-marketing/social-marketing/) accounts so that there's one central place for candidates and the community to find out more about the company as a whole.

On LinkedIn, we have a specific [career page](https://www.linkedin.com/company/gitlab-com/life) where candidates can find out more about life at GitLab. 

We are working on a "life at GitLab" video, which is to be completed in Q2. There are also a number of videos on our [YouTube channel](https://www.youtube.com/gitlab) that relate to working here:
- [Everyone can contribute](https://youtu.be/V2Z1h_2gLNU)
- [Working remotely at GitLab](https://youtu.be/NoFLJLJ7abE)
- [This is GitLab](https://youtu.be/Mkw1-Uc7V1k)
- [What is GitLab?](https://youtu.be/MqL6BMOySIQ)

### Review sites

We want to be sure candidates who come across GitLab's profile on employer review sites have an accurate picture of what it's like to work here. 
There are some sites where GitLab has a company profile, but we do not own it or pay for additional features. On others, we have a managed presence. 

We encourage team members to leave reviews and share their stories on these sites to continue to keep an updated profile. 

#### Glassdoor

**Engaged employer**

As an engaged employer with Glassdoor, we're able to customize the branded content, videos, links, and images on our Glassdoor profile. Our contract with Glassdoor includes pages in these countries:
- United States
- United Kingdom
- Germany
- The Netherlands
- India
- Ireland
- Rest of World (all countries where Glassdoor does not have a separate presence)

This also includes sponsored international jobs (all countries outside of the US and UK), as well as the ability to respond to reviews.

**Responding to reviews**

The [Talent Brand Manager](/job-families/people-ops/talent-branding-manager/) responds to reviews on a weekly basis, with input from any necessary teams internally. More details are outlined in [this Google Doc](https://docs.google.com/document/d/1slJ_C5hh_9_A078Mj8k5N5WyAXwYY45QMktCYwVBUZ8/edit?usp=sharing).

**Escalating reviews and tracking success**

Glassdoor reviews and ratings have valuable themes and trends that can help us improve our culture, hiring process, benefits, and more. 
We share ratings trends, key themes, or escalations to the appropriate contacts internally so that we can track our success and take addition action if needed. This includes:
- Recruiting team meeting - sharing hiring-related themes that have come up twice a month
- G&A metrics monthly review
- As-needed direct escalations - for reviews that require immediate action or response

**OpenCompany designation**

One of our Q2 OKRs was to reach OpenCompany status on Glassdoor. As an open, transparent company, this designation was important for us to have to best represent our employment brand. 
Achieving and maintaining OpenCompany requires that you:
- Keep company profile up to date
- Add 5-10 new photos every 12 months
- Get 5-60 new employee reviews (depending on company size) every 12 months
- Respond to 2-10 reviews (depending on company size) every 12 months
- Promote your profile with a link on your career site

#### Comparably

We have a [premium profile with Comparably](https://www.comparably.com/companies/gitlab), an employer review site that also offers recruitment marketing tools and [award programs](/handbook/people-group/employment-branding/#employer-awards-and-recognition). 

Our goal is to keep the feedback on our profile up to date by annually (Q3) sending a link to all team members where they can consider leaving feedback or a review. 

#### Other employer sites

- [RemoteHub](https://remotehub.io/gitlab)
- [Indeed](https://www.indeed.com/cmp/Gitlab-Inc/about)
- [AngelList](https://angel.co/company/gitlab/)

### GitLab blog

To give the most authentic view of life at GitLab, we encourage team members to blog about their experiences. 
You can find many of these posts in the [culture section](/blog/categories/culture/) of the GitLab blog. 

### HackerNews

We promote life at GitLab and our open vacancies on [HackerNews](https://news.ycombinator.com/). Learn more about [how this works](/handbook/hiring/vacancies/#publicizing-the-vacancy). 

## Talent brand resources

### Hiring fact sheet

The hiring fact sheet is a quick reference guide that offers a high-level overview of working at GitLab, including our latest headcount, employer awards, and blogs.
It links to a number of places in the GitLab handbook as the single source of truth. 

The fact sheet is updated monthly, so be sure to [download the latest one](https://gitlab.com/gitlab-com/people-group/recruiting/blob/master/Hiring_fact_sheet_-_2020-02.pdf).

### Article repository

There is a curated list of blog posts and articles in the recruiting team project folder that help tell the story of life at GitLab. 

[Check out the list](https://gitlab.com/gitlab-com/people-group/recruiting/blob/master/Article_repository.md) and feel free to contribute to it. 

### GitLab talent ambassadors

Spread the word about life at GitLab by becoming a GitLab talent ambassdor. 

Whether you're a hiring manager or an individual contributor, [this page](/handbook/hiring/gitlab-ambassadors) outlines the steps you can take to help represent our talent brand and attract more great people to join the team.


## Employer awards and recognition

- [Best Workplaces in 2019, Inc.](/blog/2019/05/16/building-an-award-winning-culture-at-gitlab/)
- [18 Great Companies For Millennials in the San Francisco Area](https://www.comparably.com/articles/18-great-companies-for-millennials-in-the-san-francisco-area/)
- [Best Company Culture, Comparably](https://www.comparably.com/news/best-company-culture-2019/)
- [Best Companies for Women, Comparably](https://www.comparably.com/news/best-companies-for-women-2019/)
- [Best Companies for Diversity, Comparably](https://www.comparably.com/news/best-companies-for-diversity-2019/)

There are a number of awards and recognition programs we plan to nominate GitLab for in FY21. Here's an [epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/565) with an overview of those programs.

## All-remote work

A foundational aspect of our talent brand is the flexibility that all-remote work gives our team members. Learn more about GitLab's approach to remote work on our [all-remote page](/company/culture/all-remote/). 

## Performance indicators

Here are the definitions for the performance indicators listed in the talent brand job family. 

### Glassdoor engagement

- Company rating
- Page views
- Followers
- Apply starts on jobs

### LinkedIn Talent Brand metrics

- Career page impressions
- Percentage of page visitors who view jobs
- Percentage of new hires who visit our page

### Team member engagement score

Our team member engagement or net promoter ("I would recommend GitLab as a great place to work.") score is measured annually in the Culture Amp survey. 
Here are samples of the statements team members were asked to consider:
- I would recommend GitLab as a great place to work
- GitLab motivates me to go beyond what I would in a similar role elsewhere
- I am proud to work for GitLab
- I rarely think about looking for a job at another company
- I see myself still working at GitLab in two years' time

### Team member turnover

[Defined](/handbook/people-group/people-operations-metrics/#team-member-turnover) on the People Group Metrics page.

### Team member referrals 

Overall number of job candidate referrals from GitLab team members. 

### Hires vs. plan

[Defined](/handbook/hiring/metrics/#hires-vs-plan) on the People Group Metrics page.

