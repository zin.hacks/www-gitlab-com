---
layout: markdown_page
title: "Category Direction - Wiki"
---

- TOC
{:toc}

## Wiki

| | |
| --- | --- |
| Stage | [Create](/direction/create/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2020-02-05` |



### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

Thanks for visiting the Wiki category direction page in GitLab. This page belongs to the [Knowledge](/handbook/product/categories/#knowledge-group) group of the Create stage and is maintained by Christen Dybenko ([E-Mail](mailto:cdybenko@gitlab.com)).

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Category%3AWiki) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AWiki) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for Wiki, we'd especially love to hear from you.

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

GitLab Wikis are a great way to share documentation and organize information via built-in functionality. Each GitLab project includes a Wiki rendered by [Gollum](https://github.com/gollum/gollum), and backed by a Git repository.

**Walkthrough of GitLab wikis (starts at 9 minutes):**

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/LzFRBMGl2SA?start=541" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Where we are Headed
<!-- Describe the future state for your category. What problems will you solve?
What will the category look like once you've achieved your strategy? Use narrative
techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is fully realized -->

In 2020 we'll be tackling Group Level Wikis which will give our organizations the ability to collaborate in a shared wiki that spans multiple projects. (This is currently the most upvoted issue on GitLab!) We will also be tackling issues around markdown rendering and tidying up navigation in the Wiki. 

As we look to future plans beyond 2020, we will be aiming for real time WYSIWYG collaboration (similar to Google Docs). This would solve the problem of collaborative note taking, be highly approachable for non-technical users, but have the tremendous benefits of storing the content in a portable plain text format that can be cloned, viewed and edited locally (properties of Git).

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview 
of the evolving use cases and user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->
* All personas can use Wikis for storing information
* We may need to investigae a non-technical persona as our reserach found they are often users of wikis

### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

**Next: [Group level wiki](https://gitlab.com/groups/gitlab-org/-/epics/240)** - Currently wiki's are only on the project level. Next we are movoing them to Group level and making them container agnostic so we can expand on this and add them as a Global Wiki. This sets the stage for system/global wiki's.

**Next: [Improve AsciiDoc, RDoc and reStructuredText](https://gitlab.com/groups/gitlab-org/-/epics/701)** -  Many of our custmoers rely on specific formats to support their wikis. We should make sure our wiki's suppport these formats so that they can use their Group Level wikis as a company.

**After: [Simplify wiki editing](https://gitlab.com/groups/gitlab-org/-/epics/204)** - Currently wiki editing is inconsistent with every other page, both visually and from a workflow perspective. This makes wiki less approachable for users familiar with the rest of GitLab. We should fix these inconsistencies as a priority.

**After: [Improve wiki navigation](https://gitlab.com/groups/gitlab-org/-/epics/700)** - As wikis grow with more and more content, GitLab is not providing the tools necessary to make them easy to navigate and use. This is a problem that can be resolved with support for macros and a few other small improvements.

**Later: [Deeper integration of wikis with GitLab](https://gitlab.com/groups/gitlab-org/-/epics/699)** Once the immediate useability is addressed by the epics above, next we make sure that Wiki's can embed Issues, Designs or display other objects from GitLab.

### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to 
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

Right now, we are not working on WYSIWYG and Live Editing, but instead setting the stage with our architecture and cleaning up the existing experience. We expect to dive into editing features later in 2020.

### Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). 

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

This category is currently at the Viable maturity level, and our next maturity target is 2021 (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

We currently most closely compete with **GitHub Wikis** but we would like to compete with:

- [Confluence](https://www.atlassian.com/software/confluence)
- [Notion](https://www.notion.so/)
- Google Docs


### Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

TBD

### Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

- [Group Level Wiki](https://gitlab.com/groups/gitlab-org/-/epics/240)
- [Improve AsciiDoc, RDoc and reStructuredText support in Wikis](https://gitlab.com/groups/gitlab-org/-/epics/701)

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [Add Group level Wiki](https://gitlab.com/gitlab-org/gitlab/issues/13195) (590)
- [Allow org-mode in Wiki](https://gitlab.com/gitlab-org/gitlab/issues/19688) (180)
- [Markdown Export to PDF](https://gitlab.com/gitlab-org/gitlab/issues/13932) (166)
- [Reflect next pages in sidebar](https://gitlab.com/gitlab-org/gitlab/issues/17673) (144)
- [Update wiki page (.wiki.git) from CI](https://gitlab.com/gitlab-org/gitlab/issues/16261) (133)
- [Diff functionality in Wiki](https://gitlab.com/gitlab-org/gitlab/issues/15242) (132)


### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

Due to the current state of Wikis they are not used internally.

### Top Strategy Item(s)
<!-- What's the most important thing to move your strategy forward?-->

- [Live editing](https://gitlab.com/groups/gitlab-org/-/epics/820)
